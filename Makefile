SRC_DIR 	:= src
H_DIR		:= h
BIN_DIR		:= bin
CC 		:= g++
BIN 	:= $(BIN_DIR)/mask_pii

SRC 	:= $(wildcard $(SRC_DIR)/*.cpp)
H 		:= $(wildcard $(H_DIR)/*.h)
OBJ 	= $(patsubst $(SRC_DIR)/%,$(BIN_DIR)/%,$(SRC:.cpp=.o))

CCFLAGS	= -DNDEBUG
debug: CCFLAGS = -g -DDEBUG

.PHONY: test all clean debug demo


demo:

all: $(BIN)

$(BIN_DIR)/%.o: $(SRC_DIR)/%.cpp  | $(BIN_DIR)
	$(CC) $(CCFLAGS) -c $< -o $(@) -I$(H_DIR)

$(BIN): $(OBJ)
	$(CC) $^ -o $@

$(BIN_DIR):
	mkdir $@


clean: 
	rm -f $(BIN_DIR)/*

debug: all

demo:	$(BIN)
	@chmod +x demo.sh
	@./demo.sh $(BIN)