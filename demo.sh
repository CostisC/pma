#!/bin/bash 

BIN=${1:-'./randomize_text'}
N=3

samples="
costis@10.20.30.40
210-340-50-123
OOO-OOOO_OOOO*OOOOOO
The [quick] (brown) fox jumps over the {lazy} dog
"

echo "============================================="
echo "Running $N repetitions "
echo "_____________________________________________"
echo -e "sample plaintext  \t masked text"
echo "_____________________________________________"

for i in `seq 1 $N`; do 
   echo "$samples" | while read i; do 
       [ -z "$i" ] && continue
       ${BIN} "$i"
   done
echo "_____________________________________________"
done

