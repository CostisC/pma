/*
###################################################
# 	A masking/obfuscation algorithm
#		
#	Author: Costis Contopoulos, for Atos 2020(c)
#
###################################################
*/

#ifndef _MASKENGINE_H
#define _MASKENGINE_H

#include <iostream>
#include <string>


#ifdef DEBUG
#define DEBG(X) X
#else
#define DEBG(X)
#endif

class MaskEngine
{
	// 62 printable alphanumeric symbols
	const char* symbols = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	int symbolsset_size;


	struct RandomNum {
		char num[12];
		int length;
	};

	RandomNum random_num;


	void gen_random_num();

	char map_to_symbol(int);


public:

	MaskEngine();

	std::string mask(const std::string&);



};



#endif
