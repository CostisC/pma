## PII Masking Algorithm
An algorith to mask Private Identifiable information.

---

## Directions
Clone the project and run 

make demo

---

## Output example

```
$ make demo 
=============================================
Running 3 repetitions 
_____________________________________________
sample plaintext  	 masked text
_____________________________________________
costis@10.20.30.40: 	urToBO@92.02.58.35
210-340-50-123: 	927-239-61-344
OOO-OOOO_OOOO*OOOOOO: 	C79-0ZLh_cbIl*lflK8M
The [quick] (brown) fox jumps over the {lazy} dog: 	AUG [C0J86] (DozCB) Yqx mEOUJ 1IXB UFq {pFUC} XQ6
_____________________________________________
costis@10.20.30.40: 	PeXrAV@65.37.27.31
210-340-50-123: 	599-853-04-622
OOO-OOOO_OOOO*OOOOOO: 	ClR-kXKT_6FAH*U1AfpL
The [quick] (brown) fox jumps over the {lazy} dog: 	U4p [YXDVT] (SrWVF) 77B wJaOC HPA3 1BO {NCYI} X1O
_____________________________________________
costis@10.20.30.40: 	Fom9AW@77.27.31.65
210-340-50-123: 	158-903-54-382
OOO-OOOO_OOOO*OOOOOO: 	8uD-7HUz_71Eb*HGJJp9
The [quick] (brown) fox jumps over the {lazy} dog: 	G4R [kHOxR] (aA7O8) iEE QyQJe 74FQ 1jM {ANfW} nHV
_____________________________________________

```
