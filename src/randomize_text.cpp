#include <assert.h>
#include <iostream>
#include <string>
#include "MaskEngine.h"

using namespace std;



int main (int argc, char** argv)
{
	assert(argv[1]);

	MaskEngine mEngine;

	string in (argv[1]), out;

	out = mEngine.mask(in);

	cout << in << ": \t" << out << endl;
}