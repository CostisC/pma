#include <time.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include "MaskEngine.h"


using namespace std;

MaskEngine::MaskEngine()
{
	symbolsset_size = strlen(symbols);
}

char MaskEngine::map_to_symbol(int i)
{
	assert(i < symbolsset_size);
	return (symbols[i]);
}

void MaskEngine::gen_random_num()
{
	timeval tv;
    gettimeofday(&tv, nullptr);
	srand(tv.tv_usec);
	sprintf(random_num.num, "%d", rand());	
	random_num.length = strlen(random_num.num);
	DEBG( cout << "Random: " << random_num.num << endl; )

}

string MaskEngine::mask(const string& in)
{

	string maskedOutput;
	int str_len = in.length() - 1;
	gen_random_num();

    int grp_size = 2, nPos = 0;
    int symbol_index, step;
    char buff[grp_size+1];

	for (int i=0; i<=str_len; i++)
	{

        if (isdigit(in[i]) )
        {
        	maskedOutput.push_back(random_num.num[nPos]); 
        	step = 1;
        	DEBG( cout << in[i] << " : " << random_num.num[nPos] << endl; )
        }

        else if( (in[i] >= 'a' && in[i] <= 'z') || (in[i] >= 'A' && in[i] <= 'Z'))
        {

        	memcpy(buff, random_num.num+nPos, grp_size);
        	buff[grp_size] = 0;

        	symbol_index = strtol(buff, nullptr, 10) % symbolsset_size;
        	maskedOutput.push_back (map_to_symbol(symbol_index));
        	step = grp_size;

        	DEBG( cout << in[i] << " : " << buff << " : " << symbol_index 
        		<< " : " << map_to_symbol(symbol_index) << endl; )
        }
        else 
        {
        	maskedOutput.push_back(in[i]); 
        	step = 0;
        	DEBG( cout << in[i] << " : " << in[i] << endl; )
        } 

		if ((nPos += step) >= random_num.length - 1) 
		{
			if (i == str_len) break;
			gen_random_num();
			nPos = 0;
		}

	}


 	return maskedOutput;
}

